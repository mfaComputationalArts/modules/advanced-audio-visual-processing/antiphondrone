#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
  
  //Setup things, setupGui and setupSynth are a bit of a mishmash compared to how I would normally set things
  //up as we have UI components being created in the synth section - but it works…
  _setupGui();
  _setupSynth();
  _setupSoundStream();
  
  //Make sure the xf value of all oscialtors is set from initial frequnecies
  for (_Oscillators::iterator it = _sineOsc.begin(); it != _sineOsc.end(); ++it) {
    it->freq.xf = it->freq.f;
  }
  
  //For the display on the screen, we want to show the last X pixels, which will be the dith of the display
  _displaySize = ofGetWidth() - 1;
  _displayHeight = ofGetHeight() / (double)NUM_SINE_OSC;
  
  //To try and avoid issues between the main thread and the audio out thread, we will set the vector in use
  //to have a specific size, and then never use push_back etc on it - we will just calculate a current position
  _mainDisplay.resize(_displaySize);
  
  //Setup all the OSC displays to be the same size as well
  _oscDisplays.resize(NUM_SINE_OSC);
  for (_Displays::iterator it = _oscDisplays.begin(); it != _oscDisplays.end(); ++it) {
    it->resize(_displaySize);
  }

  
  
}

//--------------------------------------------------------------
void ofApp::update(){
  
  //Lock GUI panels together - order
  //Oscilators - Cross Modulation -
  glm::vec3 p = _guiOsc->getPosition();
  float w = _guiOsc->getWidth();
  _guiCrossMod->setPosition(p.x + w, p.y);
  w += _guiCrossMod->getWidth();
  _guiFilters->setPosition(p.x + w, p.y);
  
}

//--------------------------------------------------------------
void ofApp::draw(){

  //This function was crashing when it coincided with the thread running audio out
  //making the data it uses much more consistent in size seems to have fixed, but perhaps a mutex would be a better
  //aproach, holding up drawing of a frame, and letting the audiooutput _always_ have prioirty
  
  //Not particu;lary exciting stuff here, but gives at least some feedback
  
  //Draw each output display
  ofPushMatrix();
  ofTranslate(0, _displayHeight / 2.0f);
  ofPushStyle();
  ofSetColor(ofColor::aliceBlue);

  for (_Displays::iterator it = _oscDisplays.begin(); it != _oscDisplays.end(); ++it) {
    //Loop through all points in the deque
    size_t n = MIN(_displaySize, _mainDisplay.size());
    for (size_t i = 1; i < n; i++) {
      ofDrawLine(i-1, (*it)[i-1] * _displayHeight / 2.0f, i, (*it)[i] * _displayHeight / 2.0f);
    }
    //move for next display output
    ofTranslate(0, _displayHeight);
  }
  ofPopStyle();
  ofPopMatrix();
  
  //Draw main output scope
  ofPushMatrix();
  ofPushStyle();
  ofTranslate(0, ofGetHeight() / 2.0f);
  ofSetColor(ofColor::limeGreen);
  
  ofSetLineWidth(3.0f);
  
  size_t n = MIN(_displaySize, _mainDisplay.size());

  for (size_t i = 1; i < n; i++) {
    ofDrawLine(i-1, _mainDisplay[i-1] * ofGetHeight() / 2.0f, i, _mainDisplay[i] * ofGetHeight() / 2.0f);
  }
  
  ofPopStyle();
  ofPopMatrix();
    
}



#pragma mark - Soundstream setup and callbacks

void ofApp::_setupSoundStream() {
  
  
  //Try to setup the soundstream based on the specifed device name - if we can't find
  //it then we will stop the application
  
  //Get a list of all the devices available
  vector<ofSoundDevice> devices = _soundStream.getDeviceList();
  
  vector<ofSoundDevice>::iterator it = find_if(devices.begin(), devices.end(), [&](const ofSoundDevice& d){
    return d.name == DEVICE_NAME;
  });
  
  if (it == devices.end()) {
    
    cout << "Could not find device " << DEVICE_NAME << " check loop back is running and virtual device has the right name" << endl;
    
  } else {
    cout << "Found device " << DEVICE_NAME << " will connect and setup" << endl;
    
    ofSoundStreamSettings settings;
    settings.setOutDevice(*(it));
    settings.setOutListener(this);
    settings.sampleRate = SAMPLE_RATE;
    settings.numOutputChannels = NUMBER_OUT_CHANNELS;
    settings.bufferSize = BUFFER_SIZE;
    
    _soundStream.setup(settings);
  
    //We should also setup maximilian as well
    ofxMaxiSettings::setup(SAMPLE_RATE, NUMBER_OUT_CHANNELS, BUFFER_SIZE);
    
  }
  
  
}



void ofApp::audioOut(ofSoundBuffer& output) {
  
  //We will need to loop around all the samples in the buffer
  for (size_t i = 0; i < BUFFER_SIZE; i++) {
    
    double out = 0.0f;
    double sum = 0.0f;
    
    size_t channel = 0;
    
    //not sure if this is the best apprch, but it does appear to work - we get new sample values from each oscilator in a signle loop (this makes sure everything has been advanced by one sample and only one sample in each loop).
    //Beacuse this efficly gives us the value we need to output, we actually created a summed output here, as afetr this we then adjust the cross modulation frequency, ready for this output
    for (_Oscillators::iterator it = _sineOsc.begin(); it != _sineOsc.end(); ++it, channel++) {
      //Just get next sine sample for all oscialtors for use in the next loop - this makes sure we have
      //sampled each one only once, and we never call sinbuf4 in the main loop - we use the stored sample value
      it->freq.sample = it->osc.sinebuf4(it->freq.xf);
      //By using xf - this is the value that is updated in the main loop - even if not cross mod, so we are kind of
      //a sample behind, and we can set the summed output here
      double oscOut = ( it->freq.sample * it->level->getValue() );
      sum += oscOut;
      //Update the individual wave display buffer values here, note not using push_back, and keeping the
      //vector at a constant size, so we hopefully have some more thread safety
      _oscDisplays[channel][_displayPos] = oscOut;
      //DO NOT UPDATE DISPLAY POS HERE - ITS UPDATED ONCE WE HAVE DONE THE MAIN DISPLAY AS WELL
    
    }
    
    //So now we can cross modulate - that is adjust the cross modulation frequency (xf) by each socialtor that is assigned as cross mod source, using the sample values from the last loop
    for (_Oscillators::iterator it = _sineOsc.begin(); it != _sineOsc.end(); ++it) {
      //update the cross modulation frequncies, based on what cross modulates what - this relies on the
      //crossMod.by vector being on order of how the signal is modulated
      it->freq.xf = it->freq.f;         //Start at the base frequency
      for (CrossModulatedBy::iterator cit = it->crossMod.by.begin(); cit != it->crossMod.by.end(); ++cit) {
        it->freq.xf += _sineOsc[(*cit)].freq.sample * _sineOsc[(*cit)].crossMod.range->getValue();
      }
    }
    
    //Signal processing - first divide the output by the number of oscilators
    sum /= (double)NUM_SINE_OSC;                //Scale output by number of oscilators
    
    
    //Filters
    double lpOut = _filter.lores(sum, _guiLPCutoff->getValue(), _guiLPResonance->getValue()) * 0.5;
    
    //Pass to output
    out = lpOut;
    
    //Also store output in main display for drawing - don't use push_back or anything else which
    //might change the vectors size / location in memory
    _mainDisplay[_displayPos] = out;
    _displayPos++;
    
    if (_displayPos >= _displaySize) {
      _displayPos = 0;
    }
    
    //Fill the output buffer
    size_t idx = i * NUMBER_OUT_CHANNELS;
    
    output[idx] = out;
    output[idx + 1] = out;
    
  }
  
  
}



#pragma mark - gui setup, note some of this will also happen in the synth setup

void ofApp::_setupGui() {
  
  _guiOsc = new ofxDatGui(ofxDatGuiAnchor::TOP_LEFT);
  _guiOsc->addHeader("Antiphon Drone :: Oscillators");
  
  _guiMaster = _guiOsc->addFolder("Master");
  //Master frequency control for all the oscilators
  _masterFreq.slider = _guiMaster->addSlider("frequency", 0.0, 1.0, 0.1);
  _masterFreq.slider->setPrecision(3);
  _masterFreq.range.min = 48;
  _masterFreq.range.max = 115;
  _masterFreq.f = _midiNoteToFrequency(_masterFreqToNote());
  
  _masterFreq.slider->onSliderEvent([=](ofxDatGuiSliderEvent e) {
    _masterFreq.f = _midiNoteToFrequency(_masterFreqToNote());
    //Need to update all the individual frequencies now
    for (_Oscillators::iterator it = _sineOsc.begin(); it != _sineOsc.end(); ++it) {
      it->freq.f = _sliderToFreq(it->freq.slider->getValue(), it->freq.range);
    }
  });
  
  //Master Level control - goes between 0 and 0.3, so that individual levels can add / remove 0.3
  //this should probabaly be logarthimc - will update if I have time
  _masterLevel = _guiMaster->addSlider("level", 0.0, 0.3, 0.0);
  
  _guiMaster->expand();
  
  _guiCrossMod = new ofxDatGui(ofxDatGuiAnchor::TOP_LEFT);
  _guiCrossMod->addHeader("Antiphon Drone :: Cross Modulation");
  
  _guiFilters = new ofxDatGui(ofxDatGuiAnchor::TOP_LEFT);
  _guiFilters->addHeader("Antiphon Drone :: Processing");
  
  _guiLPCutoff = _guiFilters->addSlider("LP Filter Hz", 10.0, 1000, 500);
  _guiLPResonance = _guiFilters->addSlider("LP Resonance", 1, 100, 3);
  
    
  //Because we always interogate level sliders directly for values, ofxDatGui can be very noisy in
  //the output with warnings about event handlers not set, so we turns these off.
  ofxDatGuiLog::quiet();
  
}


#pragma mark - synth stuff

void ofApp::_setupSynth() {
  
  
  
  //First create the 8 oscilators we need for the main voice
  _sineOsc.resize(NUM_SINE_OSC);
  
  int i = 0;
  for (_Oscillators::iterator it = _sineOsc.begin(); it != _sineOsc.end(); ++it, i++) {
    //Create ofxDatGui Components for each voice - these are normalised -1 to +1 attenverters on
    //top of the main frequency
    
    //Note frequency ranges are being handled as a floating point midi note, which is then converted
    //to a frequency - this is a slightly roundabout hack reusing some MIDI note to frequency code
    //from the TerrainSynth work, as attempts to map linear to log ranges were sometimes generating
    //erroneous values, and didn't have enough time to fully fix
    
    if (i < 6) {
      //normal oscilator - we want to make this adjustment reasonably small so we can get nice micro-tonal
      //tunings from the voices, and not be limited by the slider resolution - lets try an octave either side
      it->freq.range.min = -12;
      it->freq.range.max = +12;
    } else {
      //slightly larger adjustment, we are not quite modelling the very slow LFO rates of the antiphon
      //but is interesting to see if this is useful
      it->freq.range.min = -24;
      it->freq.range.max = 24;
    }
    
    //Set each slider up with some sort of random detune on it between -0.2 and 0.2 so we initialy have
    //a homogenous state (though may sound awful!)
    it->freq.slider = _guiOsc->addSlider("frequency "  + ofToString(i + 1), -1.0, 1.0, ofRandom(-0.2,0.2));
    it->freq.slider->setPrecision(3);
    
    it->freq.slider->onSliderEvent([=](ofxDatGuiSliderEvent e){
      //handler for slider change events
      it->freq.f = _sliderToFreq(e.value, it->freq.range);
    });
    
    //init frequency
    it->freq.f = _sliderToFreq(it->freq.slider->getValue(), it->freq.range);
    
    //Add a level
    it->level = _guiOsc->addSlider("level " + ofToString(i + 1), -0.3, 0.7, 0.0);
    
    //Add cross modulation
    //matrix is a really useful, but annoying control as we can have X number of buttons, but we can't easily
    //control the labels - these are numbered 1-7 in this case, and we will handle not sending to self, but means
    //numbers are a bit confusing… would be great to take this control and expand on a bit
    //really usefully though, as allows for quite complex routings
    it->crossMod.channel = _guiCrossMod->addMatrix("send from " + ofToString(i + 1) + " to ", 7, true);
    //We need to create a vector of routings each time it changes (crossMod.by)
    it->crossMod.channel->onMatrixEvent([=](ofxDatGuiMatrixEvent e){
      //We use the selected values in audio out to determine which channels are cross modulating
      //what we need to do, is updated the channel just changed to say what it's cross modulated by
      //e.child gives us what just changed, and e.enabled determines if selected or not
      //TODO: as per note in intro, maybe not changing the size of this would have helped with data sharing across threads. The order of this is also now important re the cross mod processing, which I think is corerct, but we have no visual sense of what order things are in wohich would be a useful improvment
      int osc = e.child >= 1 ? e.child + 1 : e.child;
      if (e.enabled) {
        _sineOsc[osc].crossMod.by.push_back(i);
      } else {
        CrossModulatedBy::iterator it = remove(_sineOsc[osc].crossMod.by.begin(), _sineOsc[osc].crossMod.by.end(), osc);
      }
    });
    
    //Range controls for cross modulation - this is just freuqnecy, and a fairly small range, as large values seem to trigger audio issues, which I haven't quite got to the bottom of yet
    it->crossMod.range = _guiCrossMod->addSlider("range", 0.0,200.0,0.0);
    it->crossMod.range->setPrecision(2);
    
  }
  
  
}


double ofApp::_masterFreqToNote() {
  
  //This just maps a 0 > 1 range input from the slider into a MIDI note (with decimal places) for the specified range of a voice. It's called when the master slider changes to get a MIDI note to input into the MIDI note to frequency
  return ofMap(_masterFreq.slider->getValue(), 0.0, 1.0, _masterFreq.range.min, _masterFreq.range.max);
  
}



double ofApp::_sliderToFreq(float v, const FrequencyRange &r) {
  
 //This does actual conversion for a slider, taking into account the master frequency, which is added to the note generated by an individual slider
  double note = _masterFreqToNote();
  note += ofMap(v, 0.0, 1.0, r.min, r.max);
  
  return _midiNoteToFrequency(note);
  
}


double ofApp::_midiNoteToFrequency(float note) {
  
  //Hack of MIDI note to frequency, where we accept non integer numbers to allow for
  //additional micro-tonality
  const double aFreq = 440;
  return (aFreq / 32.0) * pow(2.0, ((note - 9.0) / 12.0));
  
}



