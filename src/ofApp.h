/**
 
 # Advanced Audio Visual Processing - Courseworks 03
 
 ## Antiphon Drone - Drone voice based on the Dreadbox Antiphon DIY eurorack module
 
 ### Nathan Adams / nadam001@gold.ac.uk
 
 This is an 8 Oscilator max / OF program, intended to try and replicate the Dreadbox Antiphon module
 it aims to implment the following
 
 8 oscilators, controlled by a master frequency and individual adjustments +ve/-ve
 Cross modulation of reqwuncy between oscilators
 
 Use of a low pass filter
 
 
 
 ## Observations / Future Steps
 
 Has been a bit of a challenge to put together, and still has some issues, thoughts for the future that I would like to delve into further
 
 1. Issues with audio stream being corrupted - I think this is down to some incorrect values - for example too high a cross mod frequency getting generated unintentialy, or other odd issues, especially in the filter which generate NaN values.
 2. Foloowing on from 1. it would be good to identify a way to identify when this has occured, and then reset the audio stream, especially as the LP filter is using a history, and seems to pick up the NaN in the last X samples it uses for it's calculation
 3. There is I think an issue in my counting of the position of samples in the screen display, and storing them, as there are vertical jumps in the wave forms - it's possible it is also an odd interaction between screen size and buffer size
 4. I'm not 100% convinced the cross modulation does what I want it to - but it does do something interesting, and it does appear that stacking cross modulations does have an effect
 5. Be interesting to try changing the cross modulation ranges to have an option of ratio / fixed value, to get a more FM approach
 6. Behaviour on exit needs fixing, as occasionally objects are deleted before everything has finished processing, and the application crashes due to a bad pointer. Should look at ofSoundStream close when the application is closed perhaps?
 
 */

#pragma once

//Openframeworks includes
#include "ofMain.h"

//Add on includes
#include "ofxMaxim.h"
#include "ofxDatGui.h"

//Local Includes


class ofApp : public ofBaseApp{
  
public:
  void setup();
  void update();
  void draw();
  
  //Audio callback(s)
  void audioOut(ofSoundBuffer& output);

  
private:
  
  //Use output to audio interface on USB
  const string DEVICE_NAME = "Focusrite: Scarlett 2i4 USB";
  //const string DEVICE_NAME = "Rogue Amoeba Software, Inc.: Loopback Audio"; //loopback for audio recording

  //Device settings
  const size_t SAMPLE_RATE = 48000;
  const size_t NUMBER_OUT_CHANNELS = 2;
  const size_t BUFFER_SIZE = 512;
  
  const size_t BUFFER_N = NUMBER_OUT_CHANNELS * BUFFER_SIZE;
      
  //Sound stream
  ofSoundStream _soundStream;
  
  //Setup sound stream
  void _setupSoundStream();

  /**
   I'm using ofxDatGui for the user interface, as it offers come useful features over the standard GUI, I'd
   hoped to use the waveform monitor, and value plotter components, as a way of showing what was going on
   with the audio output, however these proved a little bit ineffective compared to using the whole screen and
   drawing on that itself
   */
  
  void _setupGui();                           //Sets up GUI shell, because the controls for each voice are held
                                              //in a vector, we actually add those controls when we set them up

  ofxDatGui* _guiOsc;                         //Individual UI Panel for OScilator controls
  ofxDatGuiFolder* _guiMaster;                //Folder / group for level and frequency
  
  ofxDatGui* _guiCrossMod;                    //UI for cross modulation controls
  
  ofxDatGui* _guiFilters;                     //Filter and components for the LP filter
  ofxDatGuiSlider* _guiLPCutoff;
  ofxDatGuiSlider* _guiLPResonance;


  /**

   Sound Generation
   
   I am aiming to replicate just a couple of key features of the Antiphon module, namely
   
   * 8 x Sine Wave Oscilators, controlled by a master frequency and level, with individual attenverter type controls for adjustment from the master
   * Flexible cross modulation where the output of any oscilator can be routed to FM any other oscilator, thus allowing chains of modulation
   * A Low Pass Filter - to add a little more interest over the original module, this is implmented as a resonant filter
  */
   
  void _setupSynth();                       //Sets up synth, creating a vcector of oscilators, and associated UI Components
  
  //The module has a lower frequency range on voices 7 & 8, this structure allows us to set different ranges for each voice
  struct FrequencyRange {
    double min;
    double max;
  };

  
  //The following structure gives each voice a base frequency, set by the GUI slider control (also in this structure), and has a cross modulation frequency, which is modulated by any other source, and is the actual value used when getting a sample from the maxiOsc (and stored in sample, so we can avoid multiple updates of the sample in a single pass)
  struct Frequency {
    double f;                                 //Frequency set by UI
    double xf;                                //Frequency after cross modulation
    ofxDatGuiSlider* slider;
    FrequencyRange range;
    double sample;
  };
    
  //A vector to define a list of cross modulation sources for a voice (we finish up knowing for each voice what it modulates (see the matrix component below), and what modulates it
  //Note: this might be something which was breaking use of shared memory between audioOut && draw calls / threads as it changes size based on how many cross modulations are running - it would make sense to keep this a fixed size and avoid push_back and erase calls on it - however moving shared data into it's own space is still a better solution in many ways
  typedef vector<int> CrossModulatedBy;

  //Following structure features the component controls for routing and rane
  struct CrossMod {
    ofxDatGuiMatrix* channel;                 //Matrix control, which will tell us where to route cross modulation to
    ofxDatGuiSlider* range;                   //Range of the XMod in Hz
    CrossModulatedBy by;                      //Which oscialtors are Xmod'ing this one - this would be ice to show the chain graphically in the future
  };
    
  //Finally the oscilator itself, gathering together everything we need using the above structures to create an entry in a vector for each oscilator - thus creating a design which could have the number of oscilators easily expanded
  struct Oscillator {
    maxiOsc osc;                              //The actual oscilator we will use for the voice!
    Frequency freq;                           //Frequency related data (see above)
    ofxDatGuiSlider* level;                   //Output level of the voice
    CrossMod crossMod;                        //Cross modulation data (see above);
  };
  
  typedef vector<Oscillator> _Oscillators;
  
  Frequency _masterFreq;                       //Master frequency which drives all other oscilators
  ofxDatGuiSlider* _masterLevel;               //Master Level
  
  const size_t NUM_SINE_OSC = 8;
  _Oscillators _sineOsc;
  
  //Frequency handling - all a bit hacky, but based on some previous things that I know work as I had issues mapping a linear slider scale to a log output for frequency. All frequency inputs are treated as though they were a MIDI note with decimal places, I can then use a standard MIDI note to frequency conversion to get trhe required log output - at some point should remove the oddities and just map to frequency directly
  double _masterFreqToNote();
  double _sliderToFreq(float v, const FrequencyRange& r);
  double _midiNoteToFrequency(float note);
  
  maxiFilter _filter;                          //For the low pass filter
  
  //Screen Output
  //Because the the audio call back is on a separate thread to the main output, we need to do something to
  //make sure we have some sort of thread safety - rather than store the data for visualisation in the main
  //sineOsc structure, we will setup dedicated buffers of values for things we want to access cross thread
  //which should mena they are not changing size / memory position - however we should avoid using
  //push_back or other such functions on a vector so that the size does remain constant
  
  //Main Display, will hold the main output
  typedef vector<double> _Display;
  _Display _mainDisplay;

  //Buffers for each of the eight oscilators
  typedef vector<_Display> _Displays;
  _Displays _oscDisplays;
  
  //Things we need to keep track of
  size_t _displayPos = 0;                         //Current position, updated by audioout only
  size_t _displaySize;                            //Size of the buffers, set in setup
  float _displayHeight;                           //Display height for each oscilator display for drawing

  
  
  
};
